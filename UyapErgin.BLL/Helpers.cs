﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using UyapErgin.DATA;
using Excel = Microsoft.Office.Interop.Excel;

namespace UyapErgin.BLL
{
    public static class Helpers
    {
        public static List<Case> ReadCsv(string strFilePath)
        {
            List<Case> cases = new List<Case>();
            using (StreamReader sr = new StreamReader(strFilePath))
            {
                while (!sr.EndOfStream)
                {
                    string[] cols = sr.ReadLine().Split(',');
                    cases.Add(new Case(cols));

                }
            }
            return cases;
        }

        public static List<Case> ReadXls(string strFilePath)
        {
            List<Case> cases = new List<Case>();
            Excel.Application excelApp = new Excel.Application();
            if (excelApp != null)
            {
                Excel.Workbook excelWorkbook = excelApp.Workbooks.Open(
                    strFilePath,
                    0,
                    true,
                    5,
                    "",
                    "",
                    true,
                    Excel.XlPlatform.xlWindows,
                    "\t",
                    false,
                    false,
                    0,
                    true,
                    1,
                    0);
                Excel.Worksheet excelWorksheet = (Excel.Worksheet)excelWorkbook.Sheets[1];

                Excel.Range excelRange = excelWorksheet.UsedRange;
                int rowCount = excelRange.Rows.Count;
                int colCount = excelRange.Columns.Count;

                for (int i = 2; i <= rowCount; i++)
                {
                    List<string> cols = new List<string>();
                    for (int j = 0; j < 7; j++)
                    {
                        Excel.Range range = (excelWorksheet.Cells[i, (j + 1)] as Excel.Range);

                        string cellValue = range.Value;
                        if (cellValue != null)
                            cols.Add(cellValue.ToString().Trim().Trim('"'));
                        else
                            cols.Add(null);
                    }
                    cases.Add(new Case(cols.ToArray()));
                }

                excelWorkbook.Close(false, Type.Missing, Type.Missing);
                excelApp.DisplayAlerts = false;
                excelApp.Quit();
            }
            return cases;
        }

        public static void CreateAndSend(List<Case> cases, string[] tos, string username, string password, string smtp, int port, string subject, string body, int notificationMinute)
        {
            MailMessage message = new MailMessage();
            foreach (string to in tos)
                message.To.Add(to);
            message.From = new MailAddress(username);
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;
            StringBuilder sb = CreateICSString(cases, username, notificationMinute);

            var calendarBytes = Encoding.UTF8.GetBytes(sb.ToString());
            MemoryStream ms = new MemoryStream(calendarBytes);
            System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(ms, "Durusmalar.ics", "text/calendar");

            message.Attachments.Add(attachment);

            using (SmtpClient client = new SmtpClient())
            {
                client.Port = port;
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = smtp;
                client.Credentials = new NetworkCredential(username, password);
                client.Send(message);
            }

        }


        public static StringBuilder CreateICSString(List<Case> cases, string mail, int notificationMinute)
        {
            StringBuilder sb = new StringBuilder();
            string DateFormat = "yyyyMMddTHHmmssZ";
            string localFormat = "yyyyMMddTHHmmss";
            string now = DateTime.Now.ToUniversalTime().ToString(DateFormat);
            DateTime minDate = cases.Min(c => c.DateTime);
            string stDate = minDate.ToString(DateFormat);
            string stLocalDate = minDate.ToString(localFormat);
            string enDate = cases.Max(c => c.DateTime).ToString(DateFormat);

            sb.AppendLine($"BEGIN:VCALENDAR");
            sb.AppendLine($"PRODID:-//Microsoft Corporation//Outlook 16.0 MIMEDIR//EN");
            sb.AppendLine($"VERSION:2.0");
            sb.AppendLine($"METHOD:PUBLISH");
            sb.AppendLine($"X-CALSTART:{stDate}");
            sb.AppendLine($"X-CALEND:{enDate}");
            sb.AppendLine($"X-WR-RELCALID:{Guid.NewGuid()}");
            sb.AppendLine($"X-WR-CALNAME:Duruşma Takvimi");
            sb.AppendLine($"X-PRIMARY-CALENDAR:TRUE");
            sb.AppendLine($"X-OWNER;CN=\"Duruşma Takvimi\":mailto:{mail}");
            sb.AppendLine($"X-MS-OLK-WKHRSTART;TZID=\"Turkey Standard Time\":080000");
            sb.AppendLine($"X-MS-OLK-WKHREND;TZID=\"Turkey Standard Time\":170000");
            sb.AppendLine($"X-MS-OLK-WKHRDAYS:MO,TU,WE,TH,FR");
            sb.AppendLine($"BEGIN:VTIMEZONE");
            sb.AppendLine($"TZID:Turkey Standard Time");
            sb.AppendLine($"BEGIN:STANDARD");
            sb.AppendLine($"DTSTART:{stLocalDate}");
            sb.AppendLine($"TZOFFSETFROM:+0300");
            sb.AppendLine($"TZOFFSETTO:+0300");
            sb.AppendLine($"END:STANDARD");
            sb.AppendLine($"END:VTIMEZONE");


            foreach (Case c in cases)
            {
                string dtStart = c.DateTime.ToString(localFormat);
                string dtEnd = c.DateTime.AddMinutes(30).ToString(localFormat);

                sb.AppendLine("BEGIN:VEVENT");
                sb.AppendLine("CLASS:PUBLIC");
                sb.AppendLine($"CREATED:{now}");
                sb.AppendLine($"DESCRIPTION:{string.Format("Katılanlar : {0}\\nDosya Türü : {1}\\nİşlem : {2}\\nSonuç : {3}\\n", c.Includers, c.FileType, c.Operation, c.Result).Replace("\"", "")}");
                sb.AppendLine($"DTEND;TZID=\"Turkey Standard Time\":{dtStart}");
                sb.AppendLine($"DTSTAMP:{now}");
                sb.AppendLine($"DTSTART;TZID=\"Turkey Standard Time\":{dtEnd}");
                sb.AppendLine($"LAST-MODIFIED:{now}");
                sb.AppendLine($"LOCATION:{c.Court.Replace("\"", "")}");
                sb.AppendLine("PRIORITY:5");
                sb.AppendLine("SEQUENCE:0");
                sb.AppendLine($"SUMMARY;LANGUAGE=tr:{c.Id.Replace("\"", "")}");
                sb.AppendLine("TRANSP:OPAQUE");
                sb.AppendLine($"UID:{Guid.NewGuid()}");
                sb.AppendLine("X-MICROSOFT-CDO-BUSYSTATUS:BUSY");
                sb.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:1");
                sb.AppendLine("X-MICROSOFT-DISALLOW-COUNTER:FALSE");
                sb.AppendLine("X-MS-OLK-AUTOFILLLOCATION:FALSE");
                sb.AppendLine("X-MS-OLK-CONFTYPE:0");
                sb.AppendLine("BEGIN:VALARM");
                sb.AppendLine($"TRIGGER:-PT{notificationMinute}M");
                sb.AppendLine("ACTION:DISPLAY");
                sb.AppendLine("DESCRIPTION:Duruşma Hatırlatıcı");
                sb.AppendLine("END:VALARM");
                sb.AppendLine("END:VEVENT");
            }

            sb.AppendLine("END:VCALENDAR");

            return sb;
        }


    }
}
