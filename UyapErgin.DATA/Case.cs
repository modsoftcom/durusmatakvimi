﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UyapErgin.DATA
{
    public class Case
    {
        public Case()
        {

        }
        public Case(string[] cols)
        {
            this.Court = cols[0].Trim().Trim('"');
            this.Id = cols[1].Trim().Trim('"');
            this.FileType = cols[2].Trim().Trim('"');
            DateTime dt;
            DateTime.TryParseExact(cols[3].Trim().Trim('"'), "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture,DateTimeStyles.None, out dt);
            this.DateTime = dt;
            this.Includers = cols[4].Trim().Trim('"');
            this.Operation = cols[5].Trim().Trim('"');
            this.Result = cols[6].Trim().Trim('"');
        }

        public string Court { get; set; }
        public string Id { get; set; }
        public string FileType { get; set; }
        public DateTime DateTime { get; set; }
        public string Includers { get; set; }
        public string Operation { get; set; }
        public string Result { get; set; }
    }
}
