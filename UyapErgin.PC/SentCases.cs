﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UyapErgin.BLL;
using UyapErgin.DATA;

namespace UyapErgin.PC
{
    public partial class SentCases : Form
    {
        public List<Case> Cases { get; set; }
        public SentCases()
        {
            InitializeComponent();
            loader.Dock = DockStyle.Fill;
        }
        protected override async void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.loader.Image = global::UyapErgin.PC.Properties.Resources.loading;
            await LoadSents();            
        }

        private async Task LoadSents()
        {
            try
            {
                loader.Show();
                loader.Update();
                Cases = new List<Case>();
                await Task.Run(() =>
                {
                    if (!string.IsNullOrEmpty(Properties.Settings.Default.SentBox))
                        Cases = JsonConvert.DeserializeObject<List<Case>>(Properties.Settings.Default.SentBox);
                });
                caseBindingSource.DataSource = Cases;
                loader.Hide();
            }
            catch (Exception ex)
            {
                
            }
        }

        private async void ctxDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bu duruşmaları gönderilenler arasından silmek istiyor musunuz?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                List<DataGridViewRow> rowsDeleted = new List<DataGridViewRow>();
                foreach (DataGridViewRow row in dtGrid.SelectedRows)
                {
                    Case @case = row.DataBoundItem as Case;
                    Cases.Remove(@case);
                }
                Properties.Settings.Default.SentBox = JsonConvert.SerializeObject(Cases);
                Properties.Settings.Default.Save();
                await LoadSents();
            }
        }

        private void ctxCreateICS_Click(object sender, EventArgs e)
        {
            List<Case> cases = new List<Case>();
            foreach (DataGridViewRow row in dtGrid.SelectedRows)
            {
                cases.Add(row.DataBoundItem as Case);
            }
            if (cases.Count == 0)
                return;
            CreateICS(cases);
        }

        private void CreateICS(List<Case> cases)
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.MailTo))
            {
                MessageBox.Show("İçerik Bilgilerini Giriniz", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                MailContent content = new MailContent();
                if (content.ShowDialog() != DialogResult.OK)
                    return;
            }
            StringBuilder sb = Helpers.CreateICSString(cases, Properties.Settings.Default.MailTo, Properties.Settings.Default.NotificationMinute);
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Dosyayı Kaydet";
            sfd.Filter = "Takvim Dosyası (*.ics)|*.ics";
            sfd.FileName = "Durusmalar";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, sb.ToString());
                if (MessageBox.Show("Takvim dosyası başarıyla oluşturuldu. Takviminize yüklemek istiyor musunuz?", "Bilgi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    System.Diagnostics.Process.Start(sfd.FileName);
            }
        }

        private async void ctxCreateICSAndSend_Click(object sender, EventArgs e)
        {
            this.loader.Image = global::UyapErgin.PC.Properties.Resources.sending;
            loader.Show();
            loader.Update();
            List<Case> cases = new List<Case>();
            foreach (DataGridViewRow row in dtGrid.SelectedRows)
            {
                cases.Add(row.DataBoundItem as Case);
            }
            if (cases.Count == 0)
                return;
            await SendICS(cases);
            loader.Hide();
        }

        private async Task SendICS(List<Case> cases)
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.MailUsername))
            {
                MessageBox.Show("E-Posta Bilgilerinizi Giriniz", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                MailSettings settings = new MailSettings();
                if (settings.ShowDialog() != DialogResult.OK)
                    return;
            }
            MailContent mailer = new MailContent(true);
            if (mailer.ShowDialog() == DialogResult.OK)
            {
                await Task.Run(() =>
                            {
                                Helpers.CreateAndSend(
                                    cases,
                                    Properties.Settings.Default.MailTo.Split(','),
                                    Properties.Settings.Default.MailUsername,
                                    Cipher.Decrypt(Properties.Settings.Default.MailPassword),
                                    Properties.Settings.Default.MailSMTP,
                                    Properties.Settings.Default.MailPort,
                                    Properties.Settings.Default.MailSubject,
                                    Properties.Settings.Default.MailBody,
                                    Properties.Settings.Default.NotificationMinute);
                            });
                List<Case> sent = JsonConvert.DeserializeObject<List<Case>>(Properties.Settings.Default.SentBox);
                sent.AddRange(cases);
                sent.Distinct();
                Properties.Settings.Default.SentBox = JsonConvert.SerializeObject(sent);
                Properties.Settings.Default.Save();
                MessageBox.Show($"Duruşmalara ait takvim dosyası {Properties.Settings.Default.MailTo} adresine gönderildi.", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
    }
}
