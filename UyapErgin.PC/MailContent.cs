﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace UyapErgin.PC
{
    public partial class MailContent : Form
    {
        public MailContent(bool isForSend = false)
        {
            InitializeComponent();
            if (isForSend)
                btnSaveSend.Text = "Gönder";
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            txtTo.Text = !string.IsNullOrEmpty(Properties.Settings.Default.MailTo) ? Properties.Settings.Default.MailTo : Properties.Settings.Default.MailUsername;
            txtSubject.Text = Properties.Settings.Default.MailSubject;
            txtBody.Text = Properties.Settings.Default.MailBody;

        }
        private void txtTo_TextChanged(object sender, EventArgs e)
        {
            TextBoxBase textBoxBase = sender as TextBoxBase;
            switch (textBoxBase.Name)
            {
                case "txtTo":
                    Properties.Settings.Default.MailTo = textBoxBase.Text;
                    break;
                case "txtSubject":
                    Properties.Settings.Default.MailSubject = textBoxBase.Text;
                    break;
                case "txtBody":
                    Properties.Settings.Default.MailBody = textBoxBase.Text;
                    break;
                default:
                    break;
            }
            Properties.Settings.Default.Save();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnSaveSend_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.MailTo = txtTo.Text;
            Properties.Settings.Default.MailSubject = txtSubject.Text;
            Properties.Settings.Default.MailBody = txtBody.Text;
            Properties.Settings.Default.Save();
            if (!string.IsNullOrEmpty(Properties.Settings.Default.MailTo)
                && !string.IsNullOrEmpty(Properties.Settings.Default.MailSubject)
                && !string.IsNullOrEmpty(Properties.Settings.Default.MailBody))
                DialogResult = DialogResult.OK;
            else
                DialogResult = DialogResult.No;
            Close();
        }
    }
}
