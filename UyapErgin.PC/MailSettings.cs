﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using UyapErgin.BLL;

namespace UyapErgin.PC
{
    public partial class MailSettings : Form
    {
        public MailSettings()
        {
            InitializeComponent();
        }

        private void MailSettings_Load(object sender, EventArgs e)
        {
            txtServer.Text = Properties.Settings.Default.MailSMTP;
            txtPort.Value = Properties.Settings.Default.MailPort;
            txtUsername.Text = Properties.Settings.Default.MailUsername;
            if (!string.IsNullOrEmpty(Properties.Settings.Default.MailPassword))
                txtPassword.Text = Cipher.Decrypt(Properties.Settings.Default.MailPassword);
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            Properties.Settings.Default.MailSMTP = txtServer.Text;
            Properties.Settings.Default.MailPort = (int)txtPort.Value;
            Properties.Settings.Default.MailUsername = txtUsername.Text;
            if (!string.IsNullOrEmpty(txtPassword.Text))
                Properties.Settings.Default.MailPassword = Cipher.Encrypt(txtPassword.Text);
            Properties.Settings.Default.Save();
            if (!string.IsNullOrEmpty(Properties.Settings.Default.MailSMTP)
                && !string.IsNullOrEmpty(Properties.Settings.Default.MailUsername)
                && !string.IsNullOrEmpty(Properties.Settings.Default.MailPassword)
                && Properties.Settings.Default.MailPort != 0)
                DialogResult = DialogResult.OK;
            else
                DialogResult = DialogResult.No;
            base.OnClosing(e);
        }

        private void btnGmail_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem toolStripItem = sender as ToolStripMenuItem;
            txtServer.Text = toolStripItem.Tag.ToString();
            txtPort.Value = 587;
            if (toolStripItem.Name.Equals("btnGmail"))
            {
                if (MessageBox.Show("Gmail adresiniz ile e-posta gönderebilmek için Gmail güvenlik ayarınızı değiştirmelisiniz. Gmail sayfasını açmak istiyor musunuz?", "Bilgi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ProcessStartInfo sInfo = new ProcessStartInfo("https://myaccount.google.com/lesssecureapps");
                    Process.Start(sInfo);
                }

            }
        }
    }
}
