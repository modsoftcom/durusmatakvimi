﻿namespace UyapErgin.PC
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuOperations = new System.Windows.Forms.ToolStripMenuItem();
            this.menuUpload = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRemoveSents = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCreateICS = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCreateAndSendICS = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSentBox = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNotification = new System.Windows.Forms.ToolStripMenuItem();
            this.btnTenMinutes = new System.Windows.Forms.ToolStripMenuItem();
            this.btnThirtyMinutes = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOneHour = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOneDay = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCustomTime = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnMailSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMailBody = new System.Windows.Forms.ToolStripMenuItem();
            this.dtGrid = new System.Windows.Forms.DataGridView();
            this.courtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ıdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ıncludersDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.operationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctxCreateICS = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxCreateICSAndSend = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.ctxDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.caseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.loader = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGrid)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.caseBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loader)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuOperations,
            this.menuSettings});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(641, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuOperations
            // 
            this.menuOperations.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuUpload,
            this.btnRemoveSents,
            this.menuCreateICS,
            this.menuCreateAndSendICS,
            this.toolStripMenuItem2,
            this.menuSentBox});
            this.menuOperations.Name = "menuOperations";
            this.menuOperations.Size = new System.Drawing.Size(60, 19);
            this.menuOperations.Text = "İşlemler";
            // 
            // menuUpload
            // 
            this.menuUpload.Name = "menuUpload";
            this.menuUpload.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.menuUpload.Size = new System.Drawing.Size(240, 22);
            this.menuUpload.Text = "Dosya Yükle";
            this.menuUpload.Click += new System.EventHandler(this.menuUpload_Click);
            // 
            // btnRemoveSents
            // 
            this.btnRemoveSents.Enabled = false;
            this.btnRemoveSents.Name = "btnRemoveSents";
            this.btnRemoveSents.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.btnRemoveSents.Size = new System.Drawing.Size(240, 22);
            this.btnRemoveSents.Text = "Gönderilmişleri Ayıkla";
            this.btnRemoveSents.Click += new System.EventHandler(this.btnRemoveSents_Click);
            // 
            // menuCreateICS
            // 
            this.menuCreateICS.Name = "menuCreateICS";
            this.menuCreateICS.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menuCreateICS.Size = new System.Drawing.Size(240, 22);
            this.menuCreateICS.Text = "Tümünü Takvime Ekle";
            this.menuCreateICS.Click += new System.EventHandler(this.menuCreateICS_Click);
            // 
            // menuCreateAndSendICS
            // 
            this.menuCreateAndSendICS.Name = "menuCreateAndSendICS";
            this.menuCreateAndSendICS.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.menuCreateAndSendICS.Size = new System.Drawing.Size(240, 22);
            this.menuCreateAndSendICS.Text = "Tümünü Gönder";
            this.menuCreateAndSendICS.Click += new System.EventHandler(this.menuCreateAndSendICS_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(237, 6);
            // 
            // menuSentBox
            // 
            this.menuSentBox.Name = "menuSentBox";
            this.menuSentBox.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.menuSentBox.Size = new System.Drawing.Size(240, 22);
            this.menuSentBox.Text = "Gönderilenler";
            this.menuSentBox.Click += new System.EventHandler(this.menuSentBox_Click);
            // 
            // menuSettings
            // 
            this.menuSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNotification,
            this.toolStripMenuItem1,
            this.btnMailSettings,
            this.btnMailBody});
            this.menuSettings.Name = "menuSettings";
            this.menuSettings.Size = new System.Drawing.Size(56, 19);
            this.menuSettings.Text = "Ayarlar";
            // 
            // menuNotification
            // 
            this.menuNotification.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnTenMinutes,
            this.btnThirtyMinutes,
            this.btnOneHour,
            this.btnOneDay,
            this.btnCustomTime});
            this.menuNotification.Name = "menuNotification";
            this.menuNotification.Size = new System.Drawing.Size(188, 22);
            this.menuNotification.Text = "Öncesi Bildirim Süresi";
            // 
            // btnTenMinutes
            // 
            this.btnTenMinutes.Name = "btnTenMinutes";
            this.btnTenMinutes.Size = new System.Drawing.Size(180, 22);
            this.btnTenMinutes.Tag = "10";
            this.btnTenMinutes.Text = "10 Dakika";
            this.btnTenMinutes.Click += new System.EventHandler(this.PreDefinedTime_Clicked);
            // 
            // btnThirtyMinutes
            // 
            this.btnThirtyMinutes.Name = "btnThirtyMinutes";
            this.btnThirtyMinutes.Size = new System.Drawing.Size(180, 22);
            this.btnThirtyMinutes.Tag = "30";
            this.btnThirtyMinutes.Text = "30 Dakika";
            this.btnThirtyMinutes.Click += new System.EventHandler(this.PreDefinedTime_Clicked);
            // 
            // btnOneHour
            // 
            this.btnOneHour.Name = "btnOneHour";
            this.btnOneHour.Size = new System.Drawing.Size(180, 22);
            this.btnOneHour.Tag = "60";
            this.btnOneHour.Text = "1 Saat";
            this.btnOneHour.Click += new System.EventHandler(this.PreDefinedTime_Clicked);
            // 
            // btnOneDay
            // 
            this.btnOneDay.Name = "btnOneDay";
            this.btnOneDay.Size = new System.Drawing.Size(180, 22);
            this.btnOneDay.Tag = "1440";
            this.btnOneDay.Text = "1 Gün";
            this.btnOneDay.Click += new System.EventHandler(this.PreDefinedTime_Clicked);
            // 
            // btnCustomTime
            // 
            this.btnCustomTime.Name = "btnCustomTime";
            this.btnCustomTime.Size = new System.Drawing.Size(180, 22);
            this.btnCustomTime.Text = "Özel";
            this.btnCustomTime.Click += new System.EventHandler(this.btnCustomTime_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(185, 6);
            // 
            // btnMailSettings
            // 
            this.btnMailSettings.Name = "btnMailSettings";
            this.btnMailSettings.Size = new System.Drawing.Size(188, 22);
            this.btnMailSettings.Text = "E-Posta Giriş Bilgileri";
            this.btnMailSettings.Click += new System.EventHandler(this.btnMailSettings_Click);
            // 
            // btnMailBody
            // 
            this.btnMailBody.Name = "btnMailBody";
            this.btnMailBody.Size = new System.Drawing.Size(188, 22);
            this.btnMailBody.Text = "E-Posta İçeriği";
            this.btnMailBody.Click += new System.EventHandler(this.btnMailBody_Click);
            // 
            // dtGrid
            // 
            this.dtGrid.AllowUserToAddRows = false;
            this.dtGrid.AutoGenerateColumns = false;
            this.dtGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.courtDataGridViewTextBoxColumn,
            this.ıdDataGridViewTextBoxColumn,
            this.fileTypeDataGridViewTextBoxColumn,
            this.dateTimeDataGridViewTextBoxColumn,
            this.ıncludersDataGridViewTextBoxColumn,
            this.operationDataGridViewTextBoxColumn,
            this.resultDataGridViewTextBoxColumn});
            this.dtGrid.ContextMenuStrip = this.contextMenuStrip1;
            this.dtGrid.DataSource = this.caseBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtGrid.GridColor = System.Drawing.Color.White;
            this.dtGrid.Location = new System.Drawing.Point(0, 25);
            this.dtGrid.Margin = new System.Windows.Forms.Padding(4);
            this.dtGrid.Name = "dtGrid";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.NullValue = "-";
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtGrid.Size = new System.Drawing.Size(641, 401);
            this.dtGrid.TabIndex = 1;
            // 
            // courtDataGridViewTextBoxColumn
            // 
            this.courtDataGridViewTextBoxColumn.DataPropertyName = "Court";
            this.courtDataGridViewTextBoxColumn.HeaderText = "MAHKEME";
            this.courtDataGridViewTextBoxColumn.Name = "courtDataGridViewTextBoxColumn";
            this.courtDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ıdDataGridViewTextBoxColumn
            // 
            this.ıdDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.ıdDataGridViewTextBoxColumn.HeaderText = "ESAS NO";
            this.ıdDataGridViewTextBoxColumn.Name = "ıdDataGridViewTextBoxColumn";
            this.ıdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fileTypeDataGridViewTextBoxColumn
            // 
            this.fileTypeDataGridViewTextBoxColumn.DataPropertyName = "FileType";
            this.fileTypeDataGridViewTextBoxColumn.HeaderText = "DOSYA TÜRÜ";
            this.fileTypeDataGridViewTextBoxColumn.Name = "fileTypeDataGridViewTextBoxColumn";
            this.fileTypeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateTimeDataGridViewTextBoxColumn
            // 
            this.dateTimeDataGridViewTextBoxColumn.DataPropertyName = "DateTime";
            this.dateTimeDataGridViewTextBoxColumn.HeaderText = "DURUŞMA ZAMANI";
            this.dateTimeDataGridViewTextBoxColumn.Name = "dateTimeDataGridViewTextBoxColumn";
            this.dateTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ıncludersDataGridViewTextBoxColumn
            // 
            this.ıncludersDataGridViewTextBoxColumn.DataPropertyName = "Includers";
            this.ıncludersDataGridViewTextBoxColumn.HeaderText = "KATILANLAR";
            this.ıncludersDataGridViewTextBoxColumn.Name = "ıncludersDataGridViewTextBoxColumn";
            this.ıncludersDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // operationDataGridViewTextBoxColumn
            // 
            this.operationDataGridViewTextBoxColumn.DataPropertyName = "Operation";
            this.operationDataGridViewTextBoxColumn.HeaderText = "İŞLEM";
            this.operationDataGridViewTextBoxColumn.Name = "operationDataGridViewTextBoxColumn";
            this.operationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // resultDataGridViewTextBoxColumn
            // 
            this.resultDataGridViewTextBoxColumn.DataPropertyName = "Result";
            this.resultDataGridViewTextBoxColumn.HeaderText = "SONUÇ";
            this.resultDataGridViewTextBoxColumn.Name = "resultDataGridViewTextBoxColumn";
            this.resultDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxCreateICS,
            this.ctxCreateICSAndSend,
            this.toolStripMenuItem3,
            this.ctxDelete});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(197, 76);
            // 
            // ctxCreateICS
            // 
            this.ctxCreateICS.Name = "ctxCreateICS";
            this.ctxCreateICS.Size = new System.Drawing.Size(196, 22);
            this.ctxCreateICS.Text = "Takvim Dosyası Oluştur";
            this.ctxCreateICS.Click += new System.EventHandler(this.ctxCreateICS_Click);
            // 
            // ctxCreateICSAndSend
            // 
            this.ctxCreateICSAndSend.Name = "ctxCreateICSAndSend";
            this.ctxCreateICSAndSend.Size = new System.Drawing.Size(196, 22);
            this.ctxCreateICSAndSend.Text = "E-Postaya Gönder";
            this.ctxCreateICSAndSend.Click += new System.EventHandler(this.ctxCreateICSAndSend_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(193, 6);
            // 
            // ctxDelete
            // 
            this.ctxDelete.Name = "ctxDelete";
            this.ctxDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.ctxDelete.Size = new System.Drawing.Size(196, 22);
            this.ctxDelete.Text = "Seçilenleri Sil";
            this.ctxDelete.Click += new System.EventHandler(this.ctxDelete_Click);
            // 
            // caseBindingSource
            // 
            this.caseBindingSource.DataSource = typeof(UyapErgin.DATA.Case);
            // 
            // loader
            // 
            this.loader.Image = global::UyapErgin.PC.Properties.Resources.loading;
            this.loader.Location = new System.Drawing.Point(206, 169);
            this.loader.Margin = new System.Windows.Forms.Padding(4);
            this.loader.Name = "loader";
            this.loader.Size = new System.Drawing.Size(284, 160);
            this.loader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.loader.TabIndex = 3;
            this.loader.TabStop = false;
            this.loader.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 426);
            this.Controls.Add(this.loader);
            this.Controls.Add(this.dtGrid);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Duruşma Takvimi";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGrid)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.caseBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuSettings;
        private System.Windows.Forms.DataGridView dtGrid;
        private System.Windows.Forms.PictureBox loader;
        private System.Windows.Forms.ToolStripMenuItem menuNotification;
        private System.Windows.Forms.ToolStripMenuItem btnMailSettings;
        private System.Windows.Forms.ToolStripMenuItem menuOperations;
        private System.Windows.Forms.ToolStripMenuItem menuUpload;
        private System.Windows.Forms.ToolStripMenuItem menuCreateICS;
        private System.Windows.Forms.ToolStripMenuItem menuCreateAndSendICS;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem menuSentBox;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem btnMailBody;
        private System.Windows.Forms.ToolStripMenuItem btnTenMinutes;
        private System.Windows.Forms.ToolStripMenuItem btnThirtyMinutes;
        private System.Windows.Forms.ToolStripMenuItem btnOneHour;
        private System.Windows.Forms.ToolStripMenuItem btnOneDay;
        private System.Windows.Forms.ToolStripMenuItem btnCustomTime;
        private System.Windows.Forms.ToolStripMenuItem btnRemoveSents;
        private System.Windows.Forms.BindingSource caseBindingSource;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ctxCreateICS;
        private System.Windows.Forms.ToolStripMenuItem ctxCreateICSAndSend;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem ctxDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn courtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ıdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ıncludersDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn operationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn resultDataGridViewTextBoxColumn;
    }
}

