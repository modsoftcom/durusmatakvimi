﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UyapErgin.PC
{
    public partial class CustomTimer : Form
    {
        public CustomTimer()
        {
            InitializeComponent();
            numMinute.Value = Properties.Settings.Default.CustomTime;
            cmbUnit.Text = Properties.Settings.Default.CustomTimeUnit;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int minute = (int)numMinute.Value;
            if (cmbUnit.SelectedIndex == 1)
                minute *= 60;
            else if (cmbUnit.SelectedIndex == 1)
                minute *= 1440;
            else if (cmbUnit.SelectedIndex == 1)
                minute *= 10080;
            else if (cmbUnit.SelectedIndex == 1)
                minute *= 43200;
            Properties.Settings.Default.NotificationMinute = minute;
            Properties.Settings.Default.CustomTime = (int)numMinute.Value;
            Properties.Settings.Default.CustomTimeUnit = cmbUnit.Text;
            Properties.Settings.Default.Save();
            DialogResult = DialogResult.OK;
        }
    }
}
