﻿using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UyapErgin.BLL;
using UyapErgin.DATA;

namespace UyapErgin.PC
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            loader.Dock = DockStyle.Fill;
            CheckingOthers();
        }
        private async void menuUpload_Click(object sender, EventArgs e)
        {
            string[] headers = { "MAHKEME", "ESAS NO", "DOSYA TÜRÜ", "DURUŞMA ZAMANI", "KATILIMCILAR", "İŞLEM", "SONUÇ" };
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Dosya Yükle";
            ofd.Filter = "Excel Dosyaları (*.xls,*.xlsx,*.csv)|*.xls;*.xlsx;*.csv";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string file = ofd.FileName;
                string[] validFileTypes = { ".xls", ".xlsx", ".csv" };
                string extension = System.IO.Path.GetExtension(file).ToLower();
                DataTable dt = new DataTable();
                for (int i = 0; i < headers.Length; i++)
                {
                    string header = headers[i];
                    Type type = typeof(string);
                    if (i == 3)
                        type = typeof(DateTime);
                    DataColumn dataColumn = new DataColumn(header, type);
                    dataColumn.ReadOnly = i == 0 || i == 1 || i == 3;
                    dt.Columns.Add(dataColumn);
                }
                await GetDataAsync(file, extension, dt);
            }
        }

        public async Task<DataTable> GetDataAsync(string file, string extension, DataTable dt)
        {
            this.loader.Image = global::UyapErgin.PC.Properties.Resources.loading;
            loader.Show();
            loader.Update();
            try
            {
                List<Case> cases = await Task.Run(() =>
                {
                    if (extension == ".csv")
                    {
                        return Helpers.ReadCsv(file);
                    }
                    else if (extension.Trim() == ".xls" || extension.Trim() == ".xlsx")
                    {
                        return Helpers.ReadXls(file);
                    }
                    else
                        return new List<Case>();
                });
                btnRemoveSents.Enabled = cases.Count > 0;
                caseBindingSource.DataSource = cases;
            }
            catch (Exception ex)
            {

            }
            loader.Hide();
            return dt;
        }

        private void CheckingOthers(ToolStripItem toolStripItem = null)
        {
            try
            {
                if (toolStripItem == null)
                    toolStripItem = menuNotification.DropDownItems.Find(Properties.Settings.Default.SelectedNotificationItem, false)[0];
                Properties.Settings.Default.SelectedNotificationItem = toolStripItem.Name;
                Properties.Settings.Default.Save();
                foreach (ToolStripMenuItem item in menuNotification.DropDownItems)
                {
                    item.Checked = item == toolStripItem;
                }
                if (toolStripItem.Name.Equals("btnCustomTime"))
                    btnCustomTime.Text = $"Özel ({Properties.Settings.Default.CustomTime} {Properties.Settings.Default.CustomTimeUnit})";
            }
            catch (Exception ex)
            {

            }
        }

        private void PreDefinedTime_Clicked(object sender, EventArgs e)
        {
            try
            {
                ToolStripItem toolStripItem = sender as ToolStripItem;
                int min = 0;
                int.TryParse(toolStripItem.Tag.ToString(), out min);
                Properties.Settings.Default.NotificationMinute = min;
                Properties.Settings.Default.Save();
                CheckingOthers(toolStripItem);
            }
            catch (Exception ex)
            {

            }
        }

        private void btnMailBody_Click(object sender, EventArgs e)
        {
            new MailContent().ShowDialog();
        }

        private async void btnRemoveSents_Click(object sender, EventArgs e)
        {
            this.loader.Image = global::UyapErgin.PC.Properties.Resources.loading;
            loader.Show();
            loader.Update();
            try
            {
                List<Case> sentCases = await Task.Run(() =>
                {
                    if (!string.IsNullOrEmpty(Properties.Settings.Default.SentBox))
                        return JsonConvert.DeserializeObject<List<Case>>(Properties.Settings.Default.SentBox);
                    else
                        return new List<Case>();
                });
                foreach (Case @case in sentCases)
                {
                    DataGridViewRow row = dtGrid.Rows
                        .Cast<DataGridViewRow>()
                        .Where(r => (r.DataBoundItem as Case).Id.Equals(@case.Id))
                        .First();
                    dtGrid.Rows.Remove(row);
                }

            }
            catch (Exception ex)
            {
            }
            loader.Hide();
            MessageBox.Show("Önceden gönderilmiş olan duruşmalar, listeden kaldırıldı.", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ctxDelete_Click(object sender, EventArgs e)
        {
            List<DataGridViewRow> rowsDeleted = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dtGrid.SelectedRows)
            {
                rowsDeleted.Add(row);
            }
            foreach (DataGridViewRow row in rowsDeleted)
            {
                dtGrid.Rows.Remove(row);
            }
        }

        private void ctxCreateICS_Click(object sender, EventArgs e)
        {
            List<Case> cases = new List<Case>();
            foreach (DataGridViewRow row in dtGrid.SelectedRows)
            {
                cases.Add(row.DataBoundItem as Case);
            }
            if (cases.Count == 0)
                return;
            CreateICS(cases);
        }

        private void CreateICS(List<Case> cases)
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.MailTo))
            {
                MessageBox.Show("İçerik Bilgilerini Giriniz", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                MailContent content = new MailContent();
                if (content.ShowDialog() != DialogResult.OK)
                    return;
            }
            StringBuilder sb = Helpers.CreateICSString(cases, Properties.Settings.Default.MailTo, Properties.Settings.Default.NotificationMinute);
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Dosyayı Kaydet";
            sfd.Filter = "Takvim Dosyası (*.ics)|*.ics";
            sfd.FileName = "Durusmalar";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, sb.ToString());
                if (MessageBox.Show("Takvim dosyası başarıyla oluşturuldu. Takviminize yüklemek istiyor musunuz?", "Bilgi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    System.Diagnostics.Process.Start(sfd.FileName);
            }
        }

        private void btnMailSettings_Click(object sender, EventArgs e)
        {
            new MailSettings().ShowDialog();
        }

        private async void ctxCreateICSAndSend_Click(object sender, EventArgs e)
        {
            this.loader.Image = global::UyapErgin.PC.Properties.Resources.sending;
            loader.Show();
            loader.Update();
            List<Case> cases = new List<Case>();
            foreach (DataGridViewRow row in dtGrid.SelectedRows)
            {
                cases.Add(row.DataBoundItem as Case);
            }
            if (cases.Count == 0)
                return;
            await SendICS(cases);
            loader.Hide();
        }

        private async Task SendICS(List<Case> cases)
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.MailUsername))
            {
                MessageBox.Show("E-Posta Bilgilerinizi Giriniz", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                MailSettings settings = new MailSettings();
                if (settings.ShowDialog() != DialogResult.OK)
                    return;
            }
            MailContent mailer = new MailContent(true);
            if (mailer.ShowDialog() == DialogResult.OK)
            {
                await Task.Run(() =>
                {
                    Helpers.CreateAndSend(
                        cases,
                        Properties.Settings.Default.MailTo.Split(','),
                        Properties.Settings.Default.MailUsername,
                        Cipher.Decrypt(Properties.Settings.Default.MailPassword),
                        Properties.Settings.Default.MailSMTP,
                        Properties.Settings.Default.MailPort,
                        Properties.Settings.Default.MailSubject,
                        Properties.Settings.Default.MailBody,
                        Properties.Settings.Default.NotificationMinute);
                });
                List<Case> sent = JsonConvert.DeserializeObject<List<Case>>(Properties.Settings.Default.SentBox);
                sent.AddRange(cases);
                sent.DistinctBy(c => c.Id);
                Properties.Settings.Default.SentBox = JsonConvert.SerializeObject(sent);
                Properties.Settings.Default.Save();
                MessageBox.Show($"Duruşmalara ait takvim dosyası {Properties.Settings.Default.MailTo} adresine gönderildi.", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }            
        }

        private void menuCreateICS_Click(object sender, EventArgs e)
        {
            this.loader.Image = global::UyapErgin.PC.Properties.Resources.loading;
            loader.Show();
            loader.Update();
            List<Case> cases = new List<Case>();
            foreach (DataGridViewRow row in dtGrid.Rows)
            {
                cases.Add(row.DataBoundItem as Case);
            }
            if (cases.Count == 0)
                return;
            CreateICS(cases);
            loader.Hide();
        }

        private async void menuCreateAndSendICS_Click(object sender, EventArgs e)
        {
            this.loader.Image = global::UyapErgin.PC.Properties.Resources.sending;
            loader.Show();
            loader.Update();
            List<Case> cases = new List<Case>();
            foreach (DataGridViewRow row in dtGrid.Rows)
            {
                cases.Add(row.DataBoundItem as Case);
            }
            if (cases.Count == 0)
                return;
            await SendICS(cases);
            loader.Hide();
        }

        private void menuSentBox_Click(object sender, EventArgs e)
        {
            new SentCases().ShowDialog();
        }

        private void btnCustomTime_Click(object sender, EventArgs e)
        {
            CustomTimer customTimer = new CustomTimer();
            if (customTimer.ShowDialog() == DialogResult.OK)
                CheckingOthers(sender as ToolStripItem);
        }
    }
}
